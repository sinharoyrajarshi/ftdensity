!  ###################################################
!  #						     #
!  #                   FTDensity                     #
!  #					             #
!  ###################################################
!  
!  This program performs spatially resolved Fourier Transform (from
!  time domaine to frequency domain) of the time-dependent induced
!  density from a delta-kick time-evolution calculation in the code
!  octopus.
!  
!  It reads a series of induced densities equally spaced in time, and
!  described in a real-space grid. At each point of the grid, it does
!  the Fourier transform of the time-dependent induced density to get
!  the sine and cosine Fourier coefficients. Finally, it assembles the
!  the calculated coefficients (sine and cosine) to reconstruct the modes
!  of electron density oscillation at any desired frequency (energy) of
!  interest. For the time being, it works for induced densitues in *dx
!  format, and might be extended to other formats easily. It has been tested
!  for output from the octopus code. [See article cited below.]
!  
!  #####################################################################################
!  
!  LICENSE:
!  
!    This program is free software: you can redistribute it and/or modify
!    it under the terms of the GNU General Public License as published by
!    the Free Software Foundation, either version 3 of the License, or
!    (at your option) any later version.
!
!    This program is distributed in the hope that it will be useful,
!    but WITHOUT ANY WARRANTY; without even the implied warranty of
!    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!    GNU General Public License for more details.
!
!    You should have received a copy of the GNU General Public License
!    along with this program.  If not, see <https://www.gnu.org/licenses/>.
!  
!  #####################################################################################
!  
!  TO ACKNOWLEDGE:
!  
!  Please cite the following article if you use this utility.
!  
!  Identifying Electronic Modes by Fourier Transform from δ‐Kick Time-Evolution
!  TDDFT Calculations.
!  R. Sinha-Roy, P. Garc\'ia-Gonzal\'ez, X. Lop\'ez Lozano, R. L. Whetten,
!  and H.-Ch. Weissker
!  DOI: http://dx.doi.org/10.1021/acs.jctc.8b00750
!  
!  #####################################################################################
!  
!  USE:
!  
!  In order to use this utility the pre-requisites are the following:
!  
!  1) FTDensity is a post-processing of the output of a delta-kick real-time
!     TDDFT calculation. Although general, it was specifically written and
!     tested using output in dx format from  the code octopus. So the directory
!     in which this utility is used should contain
!  	  a) the ground state density.dx file within static/ &
!  	  b) the time-dependent density.dx file within output_iter/.
!     As an example, these two directories having the ground state and the
!     time-dependent densities for a monatomic chain of Na are provided herewith.
!     The input files for generating these octopus output are also given.
!     These files are
!     	  i) Na_20.xyz (the system),
!  	 ii) inp_gs (input file for the static DFT calculation using octopus), &
!       iii) inp_td (input file for the time-evolution calculation using octopus).
!     They correspond to the result in the article mentioned above
!     [DOI: http://dx.doi.org/10.1021/acs.jctc.8b00750]. 
!  	  
!  2) In this version of the code, all the density files should be written
!     in dx format. However, one can easily modify the code such that output densities
!     in another format can also be used as input of this utility.
!  
!  3) FTDensity requires an input file called input. It contains 9 lines:
!     line1 : Full path to the directory having octopus output
!        	   both  /static (having gs density) & /output_iter;
!     line2 : The maximum number of time steps to be considered;
!     line3 : The step interval used in octopus td run to generate
!        	   dx files in td.0XXXXXX;
!     line4 : dt in (hbar/eV) i.e. the TDTimeStep used in octopus td run
!     line5 : Exponential damping factor (w_los) in a.u. for preparing
!        	   the input of Fourier transform. This correspond to the
!  	           gamma that is talked about in the article we mentioned
!  	           above;
!     line6 : Maximum frequency (w_max in a.u.) up to which the frequency
!     	           dependence of induced density is needed;
!     line7 : Number of frequency points;
!     line8 : Frequencies in eV (separated by space) for which the modes
!        	   are to be calculated. Maximum four value;
!     line9 : Number of time-steps to create snapshot of the
!        	   oscillating mode. This corresponds the variable t in eq 5 of
!  	           the article mentioned above.
!  
!  4) Compile the program as,
!     	   gfortran ftdensity.f90
!     Then run it,
!     	   ./a.out
!  
!  #####################################################################################
!  
!  OUTPUT:
!  
!  The programme calculates the Fourier coefficients and reconstructed modes as
!  explained in the article mentioned above.
!  
!  On the sucessful run of the code following directories are generated.
!  
!  1) COSINE_Co-eff : Contains the a(r,w) as mentioned in eq 6
!     		   of the article mentioned above;
!  2) SINE_Co-eff : Contains the b(r,w) as mentioned in eq 7 of
!     	       	 the article mentioned above;
!  3) Sq-MOD_Co-effs : Contains the S(r,w) as mentioned in eq 8
!   		    of the article mentioned above;
!  4) TD_mode_at_*****eV_index_*** : There are the directories containing
!     				    the R(r,w,t) as mentioned in eq 5 of
!				    the article metioned above.
!  
!  Each of the directories contains files in .dx format which can be visualized
!  using for instance openDX (somehow obsolete) or Chimera.
!  
!  #####################################################################################


program test_nmbrd_file
  implicit none
  integer*4 now(3), today(3)
  real x,lspr, w1, w2, w3
  integer n_max,n_max_td,nx,ny,nz,tstp,max_tstp,tstp_intrvl,i,j,k,n,AllocateStatus,n_w,prmtr,omega,line
  integer kline, fulllines, natoms, GEOM, xx, yy, zz, ilspr, iw1, iw2, iw3
  double precision x0,y0,z0,spacing,dtm,tm(0:10000),tm_max, t_cut,w_los,w_max,AVG,dw
  double precision dx, dy, dz, zeros
  character (len=90) :: flnm, slash, filename
  character(100) path, folder, folder1, folder2, folder3, makedirectory*40
  character(1000) f1, f2
  character*24 gbg, string
  REAL, DIMENSION(:, :, :, :), ALLOCATABLE :: rhot
  DOUBLE PRECISION, DIMENSION(:, :, :, :), ALLOCATABLE :: rhow, pimag, preal
  REAL, DIMENSION(:, :, :), ALLOCATABLE :: rhogs, mode
  DOUBLE PRECISION, DIMENSION(:), ALLOCATABLE :: fr, ad, den, re, im, w
  real, DIMENSION(:), ALLOCATABLE :: CHRG, GEOMX, GEOMY, GEOMZ
  integer*8 countend,countbegin,count_rate,count_max
  real readtime
  real time_elapsed_in_xdoloop
  
!----------------------------------------------------------------------------------------------
!  READING ground-state densities from the file "....../static/density.dx" and
!  then storing in the array rhogs(x,y,z).
!  Here we also read all the different information on the grid from the dx file 
!----------------------------------------------------------------------------------------------

  write(*,*) ' '
  write(*,*) ' '
  write(*,*) '          ###################################################'
  write(*,*) ' '
  write(*,*) '                              FTDensity                      '
  write(*,*) ''
  write(*,*) '          ###################################################'
  write(*,*) ' '
  write(*,*) ' '
  write(*,*) ' '
  write(*,*) '    This program is free software: you can redistribute it and/or modify'
  write(*,*) '    it under the terms of the GNU General Public License as published by'
  write(*,*) '    the Free Software Foundation, either version 3 of the License, or'
  write(*,*) '    (at your option) any later version.'
  write(*,*) ' '
  write(*,*) '    This program is distributed in the hope that it will be useful,'
  write(*,*) '    but WITHOUT ANY WARRANTY; without even the implied warranty of'
  write(*,*) '    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the'
  write(*,*) '    GNU General Public License for more details.'
  write(*,*) ' '
  write(*,*) '    You should have received a copy of the GNU General Public License'
  write(*,*) '    along with this program.  If not, see <https://www.gnu.org/licenses/>.'
  write(*,*) ' '
  write(*,*) ' '
  write(*,*) ' '
  
  open(21,file="input",status='old')
  write(*,*) ' '
  read(21,'(a)') f1
  write(*,*) 'The path for the static/ and output_iter/ is ', trim(f1)
  write(*,*) ' '
  write(*,*) 'GOING FOR: '
  write(*,*) trim(f1),'static/density.cube'
  write(*,*) ' '
  open(11,file=trim(f1)//"static/density.cube",status='old')
  read(11,*) gbg
  read(11,*) gbg
  read(11,*) natoms, x0, y0, z0
  read(11,*) nx, dx, zeros, zeros
  read(11,*) ny, zeros, dy, zeros
  read(11,*) nz, zeros, zeros, dz
  fulllines=int(nz/6)

  write(*,*) 'No. of points in each .cube file is (nx,ny,nz) = (',nx,ny,nz, ')'
  write(*,*) ' '
  write(*,*) 'Origin is set at (x0,y0,z0) in Armstrong'
  write(*,*) x0
  write(*,*) y0
  write(*,*) z0
  write(*,*) 'dx =', dx, '[in units of octopus calculation]'
  write(*,*) 'dy =', dy, '[in units of octopus calculation]'
  write(*,*) 'dz =', dz, '[in units of octopus calculation]'
  if (dx==dy .AND. dy==dz) then
     spacing=dx
     write(*,*) 'spacing = dx = dy = dz =', spacing, '[in units of octopus calculation]'
  else
     write(*,*) 'The spacing is not same in all directions... code will not work!'
  endif
  write(*,*) ' '

  ALLOCATE ( CHRG(natoms), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOMX(natoms), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOMY(natoms), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  ALLOCATE ( GEOMZ(natoms), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  write(*,*) 'The co-ordiantes of the atoms are [UnitOutput]'
  do i=1,natoms
     read(11,*) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
     write(*,*) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
  enddo
  write(*,*) ' '
  
  ALLOCATE ( rhogs(nx, ny, nz), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory ***"
  do i=1,nx
     do j=1,ny
        read(11,*) (rhogs(i,j,k), k=1,nz)
        !do k=1,nz
        !   read(11,*) rhogs(i,j,k)
        !end do
     end do
  end do
  close(11)
  !---------------------------------------------------------------------------------------------
  !              Here we take all the input information we need to run this program
  !---------------------------------------------------------------------------------------------
  read(21,*) max_tstp
  write(*,*) 'The maximum nuber of time step to be considered = ', max_tstp
  read(21,*) tstp_intrvl
  write(*,*) 'The step interval used in octopus td run to generate td.0XXXXXX/ = ', tstp_intrvl
  prmtr=tstp_intrvl
  read(21,*) dtm
  write(*,*) 'TDTimeStep in (hbar/eV) used in octopus td run = dt = ', dtm

  n_max=max_tstp/tstp_intrvl
  
  do n=0,n_max
     tm(n)=n*dtm*27.212*prmtr                 ! 'i.e. we take time in atomic units'
  end do
  tm_max=tm(n_max)  !
  write(*,*) 'Total TIME in a. u. is',tm_max
  
  t_cut = tm_max + 1.0 
  read(21,*) w_los
  write(*,*) 'Damping used (in eV) is',w_los*27.212
  read(21,*) w_max
  write(*,*) 'Maximum frequency (in eV)',w_max*27.212
  read(21,*) n_w
  write(*,*) 'No. of freq points is',n_w
  read(21,*) lspr, w1, w2, w3
  write(*,*) 'The chosen frequencies in eV are ',lspr, w1, w2, w3
  write(*,*) 'The path for the  w_***_ft-ind-den_IMAG.dx files is ', trim(f1)
  read(21,*) n_max_td
  write(*,*) 'Number of td files to be created for viewing one full oscillation of the mode is',n_max_td
  close(21)
  write(*,*) ' '

  !----------------------------------------------------------------------------------------------
  ALLOCATE ( rhot (n_max+1, nx, ny, nz), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR rhot(x,y,z,tm-stp)***"
  write(*,*) 'AllocateStatus for rhot=',AllocateStatus
  write(*,*)'(n_max, nx, ny, nz) = (', n_max+1, nx, ny, nz, ')'
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( rhow(nx, ny, nz, n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR rhow(x,y,z,n_w)***"
  write(*,*) 'AllocateStatus for rhow=',AllocateStatus
  write(*,*)'(nx, ny, n_nesc, n_w) = (', nx, ny, nz, n_w+1, ')'
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( pimag(nx, ny, nz, n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR pimag(x,y,z,n_w)***"
  write(*,*) 'AllocateStatus for pimag=',AllocateStatus
  write(*,*)'(nx, ny, n_nesc, n_w) = (', nx, ny, nz, n_w+1, ')'
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( preal(nx, ny, nz, n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR preal(x,y,z,n_w)***"
  write(*,*) 'AllocateStatus for preal=',AllocateStatus
  write(*,*)'(nx, ny, n_nesc, n_w) = (', nx, ny, nz, n_w+1, ')'
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( w(n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR w(n_w)***"
  write(*,*) 'AllocateStatus for w=',AllocateStatus
  write(*,*)'(n_w) = (',n_w+1, ')'
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( den(n_max+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR den(n_max)***"
  write(*,*) 'AllocateStatus for den=',AllocateStatus
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( fr(n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR fr(n_w)***"
   write(*,*) 'AllocateStatus for fr=',AllocateStatus 
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( ad(n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR ad(n_w)***"
  write(*,*) 'AllocateStatus for ad=',AllocateStatus
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( re(n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR re(n_w)***"
  write(*,*) 'AllocateStatus for re=',AllocateStatus
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( im(n_w+1), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR im(n_w)***"
  write(*,*) 'AllocateStatus for im=',AllocateStatus
  !----------------------------------------------------------------------------------------------
  ALLOCATE ( mode(nx, ny, nz), STAT = AllocateStatus)
  IF (AllocateStatus /= 0) STOP "*** Not enough memory FOR mode(nx, ny, nz)***"
  write(*,*) 'AllocateStatus for mode=',AllocateStatus
  !----------------------------------------------------------------------------------------------
  write(*,*)' '
  
  
  !#########################################################################################################
  !#########################################################################################################
  
  call idate(today)   ! today(1)=day, (2)=month, (3)=year
  call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
  write(*,122) today(1),today(2),today(3),now(1),now(2),now(3)  
  write(*,*) ' Reading each output_iter/td.00XXXXX/desity.cube and storing rhotd_diff(t) = [rhots(t)-rhogs] in the array as rhot(t,x,y,z).'

  call system_clock(countbegin,count_rate,count_max)  
  do tstp=1,n_max+1
     !!!! write(*,*) (tstp-1)*prmtr
     write(flnm,'(I7.7,a)') (tstp-1)*prmtr
     !!!! write(*,*) 'We are now going for density.dx in td.',flnm
     WRITE(slash,'(a)')"/"
     open(unit=379, file=trim(f1)//"output_iter/td."//trim(flnm)//trim(slash)//"density.cube" ,status='old')
     do i=1,(natoms+6)
        read(379,*) gbg
     enddo
     do i=1,nx
        do j=1,ny
           read(379,*) (rhot(tstp,i,j,k), k=1,nz) !!! Here the order of loop is [ tstp > i > j > k ]
           !do k=1,nz
           !   read(379,*) rhot(tstp,i,j,k) !!! Here the order of loop is [ tstp > i > j > k ]
           !   rhot(tstp,i,j,k)=rhot(tstp,i,j,k)-rhogs(i,j,k)
           !end do
        end do
     end do
     close(379)
     do i=1,nx
        do j=1,ny
           do k=1,nz
              rhot(tstp,i,j,k)=rhot(tstp,i,j,k)-rhogs(i,j,k)
           end do
        end do
     end do
  end do
  call system_clock(countend,count_rate,count_max)
  readtime = real(countend-countbegin) / count_rate
  write(*,*) ' Time elapsed in reading all the td CUBE files = ',readtime

  call idate(today)   ! today(1)=day, (2)=month, (3)=year
  call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
  write(*,123) today(1),today(2),today(3),now(1),now(2),now(3)
  write(*,*) ' '
  
122 FORMAT(' Started reading all  density.dx diles at DD/MM/YYYY hh:mm:ss = ',I2,'/',I2,'/',I4,' ',I2,':',I2,':',I2)
123 FORMAT(' Fnished reading all  density.dx diles at DD/MM/YYYY hh:mm:ss = ',I2,'/',I2,'/',I4,' ',I2,':',I2,':',I2)
  
  !##################################################################################################
  !                         DOING THE FOURIER TRANSFORM AND THEN WRITING IN FILES 
  !====================================================================================================
  call system_clock(countbegin,count_rate,count_max)  
  do i=1,nx
     
     do j=1,ny
        do k=1,nz
           AVG = 0.0
           do tstp=1,n_max+1
              den(tstp)=rhot(tstp,i,j,k)
              AVG = AVG+den(tstp)
           end do
           AVG = AVG/n_max  !!! write(*,*) 'The time-average of the density fluctuation at',xx,yy,zz,'is', AVG
           do tstp=1,n_max+1
              den(tstp)=den(tstp)-AVG
           enddo
           !^^^^^^^^^^^^^^ DOING FT of ONE XXX_XXX_XXX_td file ^^^^^^^^^^^^^^^^^^
           CALL ft(dtm,n_max,tm_max,tm,den,t_cut,w_los,w_max,n_w,fr,ad,re,im,i,j,k)
           !~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
           do n=1,n_w+1
              rhow(i,j,k,n) = ad(n)
              preal(i,j,k,n) = re(n)
              pimag(i,j,k,n) = im(n)
           enddo
        end do
     end do
     write(*,*) 'FT for all points (',i,'*,*) @ hh:mm:ss=',now

  enddo

  call system_clock(countend,count_rate,count_max)
  time_elapsed_in_xdoloop = real(countend-countbegin) / count_rate
  write(*,*) ' '
  write(*,*) ' Time_elapsed_in_xdoloop = ',time_elapsed_in_xdoloop
  write(*,*) ' '
  
  !#########################################################################################################
  !#########################################################################################################

!-----------------------------------------------------------------------------------------------------------------
136 FORMAT('  ',E12.6 ,'  ', E12.6,'  ',  E12.6, '  ', E12.6, '  ', E12.6, '  ', E12.6)
135 FORMAT('  ',E12.6 ,'  ', E12.6,'  ',  E12.6, '  ', E12.6, '  ', E12.6)
134 FORMAT('  ',E12.6 ,'  ', E12.6,'  ',  E12.6, '  ', E12.6)
133 FORMAT('  ',E12.6 ,'  ', E12.6,'  ',  E12.6)
132 FORMAT('  ',E12.6 ,'  ', E12.6)
131 FORMAT('  ',E12.6)
130 FORMAT('on ',I2,'/',I2,'/',I4,' ',I2,':',I2,':',I2)
129 FORMAT(I5,' ',F12.6,' ',F12.6,' ',F12.6)
128 FORMAT(I5,' ',F12.6,' ',F12.6,' ',F12.6,' ',F12.6)
!-----------------------------------------------------------------------------------------------------------------
  
  !####################################################################################################
  !                         WRITING for CHECK with a td.XXXXXXXX/density.dx file 
  !====================================================================================================
  tstp=3 !! It will create the .dx file same as the density.dx for time-step = (tstp-1)*prmtr
  write(flnm,'(I7.7)') (tstp-1)*prmtr
  open(unit=77,file=trim(flnm)//'.cube',status='unknown')
  write(77,*) 'Generated by ftdensityOMEGAremovedCUBE'
  write(77,130) today(1),today(2),today(3),now(1),now(2),now(3)
  write(77,129) natoms, x0, y0, z0
  write(77,129) nx, dx, zeros, zeros
  write(77,129) ny, zeros, dy, zeros
  write(77,129) nz, zeros, zeros, dz
  do i=1,natoms
     write(77,128) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
  enddo
  do i=1,nx
     do j=1,ny
        do k=1,nz
           mode(i,j,k) = rhot(tstp,i,j,k)+rhogs(i,j,k)
        end do
     end do
  enddo
  do xx=1,nx
     do yy=1,ny
        do kline=1,fulllines
           write(77,136) mode(xx,yy,((kline-1)*6)+1), mode(xx,yy,((kline-1)*6)+2), mode(xx,yy,((kline-1)*6)+3), mode(xx,yy,((kline-1)*6)+4), mode(xx,yy,((kline-1)*6)+5), mode(xx,yy,((kline-1)*6)+6)
        enddo
        if(MOD(nz,6)==5) write(77,135) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3), mode(xx,yy,(fulllines*6)+4), mode(xx,yy,(fulllines*6)+5)
        if(MOD(nz,6)==4) write(77,134) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3), mode(xx,yy,(fulllines*6)+4)
        if(MOD(nz,6)==3) write(77,133) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3)
        if(MOD(nz,6)==2) write(77,132) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2)
        if(MOD(nz,6)==1) write(77,131) mode(xx,yy,(fulllines*6)+1)
     end do
  end do
  close(77)
  WRITE(*,*) '------------------------------------------------------------------------------------'
  WRITE(*,*) 'If the program worked correctly the file ',trim(flnm)//'.cube',' should contain'
  WRITE(*,*) 'same data as in the file output_iter/td.'//trim(flnm)//trim(slash)//'density.cube.'
  WRITE(*,*) '------------------------------------------------------------------------------------'
  write(*,*) ' '
  !####################################################################################################

  
  !=========================================================================================================
  !#########################################################################################################
  !=========================================================================================================
  
  !=======================================Creating frequency POINTS ===================================
  dw=w_max/dfloat(n_w)
  write(*,*) 'Creating all the frequency points.'
  do n=1,n_w+1
     w(n)=dfloat(n)*dw*27.212 !Creating frequency POINTS in eV
     !write(*,*) n, w(n)
  end do
  write(*,*) ' '
  !=======================================Creating Directories for Re Im & Sq-mod======================
  path = './'
  folder1 = 'COSINE_Co-effs'
  makedirectory = 'mkdir ' // trim(path) // trim(folder1)
  print*, makedirectory
  call system(makedirectory)
  folder2 = 'SINE_Co-effs'
  makedirectory = 'mkdir ' // trim(path) // trim(folder2)
  print*, makedirectory
  call system(makedirectory)
  folder3 = 'Sq-MOD_Co-effs'
  makedirectory = 'mkdir ' // trim(path) // trim(folder3)
  print*, makedirectory
  call system(makedirectory)

 !====================================================================================================
  !                      OPENNING & WRITING THE HEADER OF THE .dx FILES
  !====================================================================================================  
  write(*,*) 'OPENNING & WRITING THE HEADER OF THE index_***_w_*.*****eV_IMAG.cube FILES'
  do omega=1,n_w+1
     x = omega*dw*27.212                   
     write( string, '(f7.5)' )  x      
     !write(*,*) 'string is ', trim(string)
     write(filename,'(I3.3)') omega
     open(unit=omega*10+14, file="SINE_Co-effs/index_"//trim(filename)//"_w_"//trim(string)//"eV_IMAG.cube",status='unknown')
     write(omega*10+14,*) 'Generated by ftdensityOMEGAremovedCUBE'
     write(omega*10+14,130) today(1),today(2),today(3),now(1),now(2),now(3)
     write(omega*10+14,129) natoms, x0, y0, z0
     write(omega*10+14,129) nx, dx, zeros, zeros
     write(omega*10+14,129) ny, zeros, dy, zeros
     write(omega*10+14,129) nz, zeros, zeros, dz
     do i=1,natoms
        write(omega*10+14,128) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
     enddo
  enddo
  write(*,*) ' '
  !====================================================================================================
  write(*,*) 'OPENNING & WRITING THE HEADER OF THE index_***_w_*.*****eV_REAL.cube FILES'
  do omega=1,n_w+1
     x = omega*dw*27.212                   
     write( string, '(f7.5)' )  x      
     !write(*,*) 'string is ', trim(string)
     write(filename,'(I3.3)') omega
     open(unit=omega*10+13, file="COSINE_Co-effs/index_"//trim(filename)//"_w_"//trim(string)//"eV_REAL.cube",status='unknown')
     write(omega*10+13,*) 'Generated by ftdensityOMEGAremovedCUBE'
     write(omega*10+13,130) today(1),today(2),today(3),now(1),now(2),now(3)
     write(omega*10+13,129) natoms, x0, y0, z0
     write(omega*10+13,129) nx, dx, zeros, zeros
     write(omega*10+13,129) ny, zeros, dy, zeros
     write(omega*10+13,129) nz, zeros, zeros, dz
     do i=1,natoms
        write(omega*10+13,128) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
     enddo
  enddo
  write(*,*) ' '
  !====================================================================================================
  write(*,*) 'OPENNING & WRITING THE HEADER OF THE index_***_w_*.*****eV_Sq-MOD.cube FILES'
  do omega=1,n_w+1
     x = omega*dw*27.212                   
     write( string, '(f7.5)' )  x      
     !write(*,*) 'string is ', trim(string)
     write(filename,'(I3.3)') omega
     open(unit=omega*10+15, file="Sq-MOD_Co-effs/index_"//trim(filename)//"_w_"//trim(string)//"eV_Sq-MOD.cube",status='unknown')
     write(omega*10+15,*) 'Generated by ftdensityOMEGAremovedCUBE'
     write(omega*10+15,130) today(1),today(2),today(3),now(1),now(2),now(3)
     write(omega*10+15,129) natoms, x0, y0, z0
     write(omega*10+15,129) nx, dx, zeros, zeros
     write(omega*10+15,129) ny, zeros, dy, zeros
     write(omega*10+15,129) nz, zeros, zeros, dz
     do i=1,natoms
        write(omega*10+15,128) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
     enddo
  enddo
  write(*,*) ' '
  !====================================================================================================
  !                       WRITING into openned freq.dx files
  !====================================================================================================
  write(*,*) 'WRITING IMAG co-effs to opened index_***_w_*.*****eV_ft-ind-den_IMAG.cube files'
  do omega=1,n_w+1
     do xx=1,nx
        do yy=1,ny
           !do zz=1,nz
           !   write(omega*10+14,*) ' ', pimag(xx,yy,zz,omega)
           !enddo
           do kline=1,fulllines
              write(omega*10+14,136) pimag(xx,yy,((kline-1)*6)+1,omega), pimag(xx,yy,((kline-1)*6)+2,omega), pimag(xx,yy,((kline-1)*6)+3,omega), pimag(xx,yy,((kline-1)*6)+4,omega), pimag(xx,yy,((kline-1)*6)+5,omega), pimag(xx,yy,((kline-1)*6)+6,omega)
           enddo
           if(MOD(nz,6)==5) write(omega*10+14,135) pimag(xx,yy,(fulllines*6)+1,omega), pimag(xx,yy,(fulllines*6)+2,omega), pimag(xx,yy,(fulllines*6)+3,omega), pimag(xx,yy,(fulllines*6)+4,omega), pimag(xx,yy,(fulllines*6)+5,omega)
           if(MOD(nz,6)==4) write(omega*10+14,134) pimag(xx,yy,(fulllines*6)+1,omega), pimag(xx,yy,(fulllines*6)+2,omega), pimag(xx,yy,(fulllines*6)+3,omega), pimag(xx,yy,(fulllines*6)+4,omega)
           if(MOD(nz,6)==3) write(omega*10+14,133) pimag(xx,yy,(fulllines*6)+1,omega), pimag(xx,yy,(fulllines*6)+2,omega), pimag(xx,yy,(fulllines*6)+3,omega)
           if(MOD(nz,6)==2) write(omega*10+14,132) pimag(xx,yy,(fulllines*6)+1,omega), pimag(xx,yy,(fulllines*6)+2,omega)
           if(MOD(nz,6)==1) write(omega*10+14,131) pimag(xx,yy,(fulllines*6)+1,omega)
        enddo
     end do
     close(omega*10+14)
     call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
     write(*,*) 'Done for omega-eV=',omega*dw*27.212,' @ hh:mm:ss=',now
  end do
  write(*,*) ' '
  !====================================================================================================
  write(*,*) 'WRITING REAL co-effs to opened index_***_w_*.*****eV_ft-ind-den_REAL.cube files'
  do omega=1,n_w+1
     do xx=1,nx
        do yy=1,ny
           !do zz=1,nz
           !   write(omega*10+13,*) preal(xx,yy,zz,omega)
           !enddo
           do kline=1,fulllines
              write(omega*10+13,136) preal(xx,yy,((kline-1)*6)+1,omega), preal(xx,yy,((kline-1)*6)+2,omega), preal(xx,yy,((kline-1)*6)+3,omega), preal(xx,yy,((kline-1)*6)+4,omega), preal(xx,yy,((kline-1)*6)+5,omega), preal(xx,yy,((kline-1)*6)+6,omega)
           enddo
           if(MOD(nz,6)==5) write(omega*10+13,135) preal(xx,yy,(fulllines*6)+1,omega), preal(xx,yy,(fulllines*6)+2,omega), preal(xx,yy,(fulllines*6)+3,omega), preal(xx,yy,(fulllines*6)+4,omega), preal(xx,yy,(fulllines*6)+5,omega)
           if(MOD(nz,6)==4) write(omega*10+13,134) preal(xx,yy,(fulllines*6)+1,omega), preal(xx,yy,(fulllines*6)+2,omega), preal(xx,yy,(fulllines*6)+3,omega), preal(xx,yy,(fulllines*6)+4,omega)
           if(MOD(nz,6)==3) write(omega*10+13,133) preal(xx,yy,(fulllines*6)+1,omega), preal(xx,yy,(fulllines*6)+2,omega), preal(xx,yy,(fulllines*6)+3,omega)
           if(MOD(nz,6)==2) write(omega*10+13,132) preal(xx,yy,(fulllines*6)+1,omega), preal(xx,yy,(fulllines*6)+2,omega)
           if(MOD(nz,6)==1) write(omega*10+13,131) preal(xx,yy,(fulllines*6)+1,omega)
        enddo
     end do
     close(omega*10+13)
     call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
     write(*,*) 'Done for omega-eV=',omega*dw*27.212,' @ hh:mm:ss=',now
  end do
  write(*,*) ' '
  !====================================================================================================
  write(*,*) 'WRITING Sq-MOD of co-effs to opened index_***_w_*.*****eV_ft-ind-den_Sq-MOD.cube files'
  do omega=1,n_w+1
     do xx=1,nx
        do yy=1,ny
           !do zz=1,nz
           !   write(omega*10+15,*) rhow(xx,yy,zz,omega)
           !enddo
           do kline=1,fulllines
              write(omega*10+15,136) rhow(xx,yy,((kline-1)*6)+1,omega), rhow(xx,yy,((kline-1)*6)+2,omega), rhow(xx,yy,((kline-1)*6)+3,omega), rhow(xx,yy,((kline-1)*6)+4,omega), rhow(xx,yy,((kline-1)*6)+5,omega), rhow(xx,yy,((kline-1)*6)+6,omega)
           enddo
           if(MOD(nz,6)==5) write(omega*10+15,135) rhow(xx,yy,(fulllines*6)+1,omega), rhow(xx,yy,(fulllines*6)+2,omega), rhow(xx,yy,(fulllines*6)+3,omega), rhow(xx,yy,(fulllines*6)+4,omega), rhow(xx,yy,(fulllines*6)+5,omega)
           if(MOD(nz,6)==4) write(omega*10+15,134) rhow(xx,yy,(fulllines*6)+1,omega), rhow(xx,yy,(fulllines*6)+2,omega), rhow(xx,yy,(fulllines*6)+3,omega), rhow(xx,yy,(fulllines*6)+4,omega)
           if(MOD(nz,6)==3) write(omega*10+15,133) rhow(xx,yy,(fulllines*6)+1,omega), rhow(xx,yy,(fulllines*6)+2,omega), rhow(xx,yy,(fulllines*6)+3,omega)
           if(MOD(nz,6)==2) write(omega*10+15,132) rhow(xx,yy,(fulllines*6)+1,omega), rhow(xx,yy,(fulllines*6)+2,omega)
           if(MOD(nz,6)==1) write(omega*10+15,131) rhow(xx,yy,(fulllines*6)+1,omega)
        enddo
     end do
     close(omega*10+15)
     call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
     write(*,*) 'Done for omega-eV=',omega*dw*27.212,' @ hh:mm:ss=',now
  end do
  write(*,*) ' '

  !=========================================================================================================
  !#########################################################################################################
  !=========================================================================================================

  

  !#####################################################################################################################
  !######################                            Making Modes                          #############################
  !######################                              Started                             #############################
  !#####################################################################################################################
  w_max=w_max*27.212  !! W_MAX in eV
  dw=w_max/n_w        !! dw in eV
  ilspr=lspr/dw       !! index of lspr filename
  iw1=w1/dw           !! index of w1 filename
  iw2=w2/dw           !! index of w2 filename
  iw3=w3/dw           !! index of w3 filename
  !########################################################################################################
  do n=1,n_w+1
     if ((n .EQ. ilspr) .OR.(n .EQ. iw1) .OR. (n .EQ. iw2) .OR. (n .EQ. iw3) ) then
        path = './'
        write(f2,'(f4.2)') n*dw
        write(flnm,'(I3.3)') n
        folder = 'TD_mode_at_'//trim(f2)//'eV'//'_index_'//trim(flnm)
        slash = '/'
        makedirectory = 'mkdir ' // trim(path) // trim(folder)
        print*, makedirectory
        call system(makedirectory)
        tm_max= 2*3.14159265359/(n*dw)
        dtm=tm_max/n_max_td
        do tstp=0,n_max_td
           write(filename,'(I3.3,a)') tstp
           open(unit=10*tstp+37, file=trim(path)//trim(folder)//trim(slash)//trim(filename)//"_density.cube" ,status='unknown')
           write(10*tstp+37,*) 'Generated by ftdensityOMEGAremovedCUBE'
           write(10*tstp+37,130) today(1),today(2),today(3),now(1),now(2),now(3)
           write(10*tstp+37,129) natoms, x0, y0, z0
           write(10*tstp+37,129) nx, dx, zeros, zeros
           write(10*tstp+37,129) ny, zeros, dy, zeros
           write(10*tstp+37,129) nz, zeros, zeros, dz
           do i=1,natoms
              write(10*tstp+37,128) GEOM, CHRG(i), GEOMX(i), GEOMY(i), GEOMZ(i)
           enddo
           do i=1,nx
              do j=1,ny
                 do k=1,nz
                    mode(i,j,k) = preal(i,j,k,n)*cos(n*dw*tstp*dtm) + pimag(i,j,k,n)*sin(n*dw*tstp*dtm)
                 end do
              end do
           end do
           do xx=1,nx
              do yy=1,ny
                 do kline=1,fulllines
                    write(10*tstp+37,136) mode(xx,yy,((kline-1)*6)+1), mode(xx,yy,((kline-1)*6)+2), mode(xx,yy,((kline-1)*6)+3), mode(xx,yy,((kline-1)*6)+4), mode(xx,yy,((kline-1)*6)+5), mode(xx,yy,((kline-1)*6)+6)
                 enddo
                 if(MOD(nz,6)==5) write(10*tstp+37,135) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3), mode(xx,yy,(fulllines*6)+4), mode(xx,yy,(fulllines*6)+5)
                 if(MOD(nz,6)==4) write(10*tstp+37,134) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3), mode(xx,yy,(fulllines*6)+4)
                 if(MOD(nz,6)==3) write(10*tstp+37,133) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2), mode(xx,yy,(fulllines*6)+3)
                 if(MOD(nz,6)==2) write(10*tstp+37,132) mode(xx,yy,(fulllines*6)+1), mode(xx,yy,(fulllines*6)+2)
                 if(MOD(nz,6)==1) write(10*tstp+37,131) mode(xx,yy,(fulllines*6)+1)
              end do
           end do
           close(10*tstp+37)
           call itime(now)     ! now(1)=hour, (2)=minute, (3)=second
           write(*,*) n*dw,": WRITING for tstp=",tstp,"FINISHED @ hh:mm:ss=",now
           write(*,*) ' '
        end do
     endif
  enddo
  !#####################################################################################################################
  !######################                            Making Modes                          #############################
  !######################                              Finished                            #############################
  !#####################################################################################################################
  
end program test_nmbrd_file










!|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
!|||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
!=========================================================================
!          SUBROUTINE FOR FOURIER TRANSFORM
!=========================================================================

SUBROUTINE ft(dt,nmax,tmax,t,dip,tcut,wlos,wmax,nw,w,ard,redip,imdip,ii,jj,kk)
  integer nmax,nw,n,i,ii,jj,kk
  double precision t(0:2000),dip(0:2000),df(0:2000),sf(0:2000),ift(0:2000)
  double precision w(0:500),redip(0:500),imdip(0:500)
  double precision dt,tcut,wlos,wmax,dw,tmax,adum,bdum,wdum,ard(0:500)

  do n=0,nmax
     df(n)=exp(-t(n)*wlos)
     !!     write(1,*) t(n),dip(n),dip(n)*df(n)
  end do

  
  !-------------------------------------------------------------------------
  !     Fourier transform is done using a simpson integrator.
  ! -------------------------------------------------------------------------
  adum=dt/3.d0
  sf(0)=adum
  sf(nmax)=adum
  adum=2.d0*adum
  do n=2,nmax-2,2
     sf(n)=adum
  end do
  adum=2.d0*adum
  do n=1,nmax-1,2
     sf(n)=adum
  end do
  !--------------------- Creating frequency POINTS -------------------------
  dw=wmax/dfloat(nw)
  do n=0,nw
     w(n)=dfloat(n)*dw
  end do
  !--------------------- Creating input for FT -----------------------------
  do n=0,nmax
     ift(n)=0.0917012*sf(n)*dip(n)*df(n)
  end do
  !--------------------- Doing SIMPSON INT. for FT -------------------------
  do i=0,nw
     adum=0.d0
     bdum=0.d0
     wdum=w(i)
     do n=1,nmax
        adum=adum+ift(n)*dcos(wdum*t(n))
        bdum=bdum+ift(n)*dsin(wdum*t(n))
     end do
     !----------------------------------------------------------------------
     !        Note that we multiply the result by the frequency to have the
     !        absorption cross section
     !----------------------------------------------------------------------          
     redip(i)=-adum !!!*w(i)
     imdip(i)=-bdum !!!*w(i)
  end do
  
  !----------------------------------------------------------------------
  !     The output: frequency, Real, Imaginay, Modulus
  !     The actual absorption cross section is the third column
  !----------------------------------------------------------------------
  do n=0,nw
     adum=redip(n)
     bdum=imdip(n)
     ard(n)=dsqrt(adum**2+bdum**2)
     w(n)=w(n)*27.212
     !!     write(1,*) w(n),adum,bdum,ard(n)
  end do
  RETURN
end SUBROUTINE ft
